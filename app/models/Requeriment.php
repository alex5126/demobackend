<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Requeriment extends Model
{
    protected $fillable = [
        'fecha',
        'type',
        'status',
        'code',
        'justification',
        'area',
        'estimatedAmount'
    ];

    public function descriptions()
    {
        return $this->hasMany(Description::class);
    }
    
}
