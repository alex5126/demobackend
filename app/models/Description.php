<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Description extends Model
{
    protected $fillable = [
        'requeriment_id',
        'name',
        'unit',
        'quantity'
    ];

    public function requeriment()
    {
        return $this->belongsTo(Requeriment::class);
    }
}
