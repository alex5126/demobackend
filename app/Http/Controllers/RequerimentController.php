<?php

namespace App\Http\Controllers;

//use App\Requeriment;
use Illuminate\Http\Request;
use App\models\Description;
use App\models\Requeriment;
use App\Http\Resources\RequerimentResource;
use App\Http\Resources\DescriptionResource;

class RequerimentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RequerimentResource::collection(\App\models\Requeriment::paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'fecha'             =>  $request->Fecha,
            'type'              =>  $request->Type,
            'status'            => $request->Status,
            'code'              =>  $request->Code,
            'justification'     =>  $request->Justification,
            'area'              =>  $request->Area,
            'estimatedAmount'   =>  $request->EstimatedAmount
        ];

        $requeriment = Requeriment::create($data); 

        foreach($request->Description as $req){
            if($req['Name'] != null){
            $description = [
                'requeriment_id'    => $requeriment->id,
                'name'  => $req['Name'],
                'unit'  => $req['Unit'],
                'quantity' => $req['Quantity']
            ];
            Description::create($description);
        }
        }        

        return new RequerimentResource($requeriment);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requeriment  $requeriment
     * @return \Illuminate\Http\Response
     */
    public function show(Requeriment $requeriment)
    {
        return new RequerimentResource($requeriment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requeriment  $requeriment
     * @return \Illuminate\Http\Response
     */
    public function edit(Requeriment $requeriment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requeriment  $requeriment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Requeriment $requeriment)
    {
        $req = Requeriment::where('id', '=',$request->id)->first();
        $req->status = $request->Status;
        $req->save();

        return new RequerimentResource($req);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requeriment  $requeriment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requeriment $requeriment)
    {
        
    }
}
