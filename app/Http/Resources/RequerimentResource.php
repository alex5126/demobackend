<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RequerimentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'Fecha'         => $this->fecha,
            'Type'          => $this->type,
            'Code'          => $this->code,
            'Status'        => $this->status,
            'Justification' => $this->justification,
            'Area'          => $this->area,
            'EstimatedAmount'=> $this->estimatedAmount,
            'Description'   => DescriptionResource::collection($this->descriptions)
        ];
    }
}
